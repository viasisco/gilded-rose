package com.ba.gildedrose;

import com.ba.gildedrose.Item.*;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;


/**
 * Units test 
 */
public class AppTest extends TestCase 
{
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public AppTest( String testName )
    {
        super( testName );
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite()
    {
        return new TestSuite( AppTest.class );
	}
	
	protected void setUp() {
	}

    /**
     * Rigourous Test :-) (by maven...)
     */
    public void testApp()
    {
        assertTrue( true );
    }

	/**
	 * Simple insert
	 */
	public void testInsertValue() 
	{
		Item item = new Item("+5 Dexterity Vest", 10, 20);
		assertEquals(20, item.GetQuality());
	}
	
	/**
	 * degrade after sellin increase
	 */
	public void testQualityTrullyDegrade() 
	{
		Item [] items = new Item [] { new Item("+5 Dexterity Vest", 10, 20)};
		GildedRose app = new GildedRose(items);
		app.updateQuality();
		assertEquals(19, items[0].GetQuality());
	}
	
	/**
	 * Once the sell by date has passed, Quality degrades twice as fast
	 */
	public void testQualityTrullyDegradeByTwice() 
	{
		Item [] items = new Item [] { new Item("+5 Dexterity Vest", 10, 20)};
		GildedRose app = new GildedRose(items);
		int days = items[0].GetSellin();
		for(int i = 0; i <  days + 1 ; i++ ) { //add one day more after sellin
			app.updateQuality();
		}
		assertEquals(4, items[0].GetQuality());
	}
	
	/**
	 * The Quality of an item is never negative
	 */
	public void testQualityNeverNegative() 
	{
		Item [] items = new Item [] { new Item("+5 Dexterity Vest", 10, 20)};
		GildedRose app = new GildedRose(items);
		int days = items[0].GetSellin();
		for(int i = 0; i <  days + 6 ; i++ ) { //add more days more to decrease quality under 0. Since days + 5, the quality equals to 0.  
			app.updateQuality();
		}
		assertEquals(0, items[0].GetQuality());	
	}
	
	/**
	 * "Aged Brie" actually increases in Quality the older it gets
	 */
	public void testAgedBrieIncreaseQuality() 
	{
		Item [] items = new Item [] {  new Item("Aged Brie", 2, 0)};
		GildedRose app = new GildedRose(items);
		int days = items[0].GetSellin();
		for(int i = 0; i <  days  ; i++ ) {   
			app.updateQuality();
		}
		assertEquals(2, items[0].GetQuality());	
	}
	
	/**
	 * The Quality of an item is never more than 50
	 */
	public void testQualityItemNeverExceedFifty() 
	{
		Item  items = new Item("+5 Dexterity Vest", 10, 50);
		BackstagePass backstagePass = new BackstagePass("Backstage passes to a TAFKAL80ETC concert", 5, 50);
		ConjuredItem conjuredItem = new ConjuredItem("Conjured Mana Cake", 3, 50);
		items.UpdateItemQuality();
		backstagePass.UpdateItemQuality();
		conjuredItem.UpdateItemQuality();
		assertEquals(true, (items.GetQuality() <= 50 && backstagePass.GetQuality() <= 50 && conjuredItem.GetQuality() <= 50) );
	}
	
	/**
	 * "Sulfuras", being a legendary item, never has to be sold or decreases in Quality
	 */
	public void testLegendaryNeverSold() 
	{
		Item [] items = new Item [] { new LegendaryItem("Sulfuras, Hand of Ragnaros", 0, 20)};
		GildedRose app = new GildedRose(items);
		for(int i = 0; i <  2  ; i++ ) { //add more days more to decrease quality under 0. Since days + 5, the quality equals to 0.  
			app.updateQuality();
		}
		assertEquals(true, !items[0].Salable() && (items[0].GetQuality() == 80));
	}

	/**
	 * verify if other items are sellable
	 */
	public void testItemIsSellable() 
	{
		Item  genericItem = new Item("+5 Dexterity Vest", 10, 20);
		Item conjuredItem = new ConjuredItem("Conjured Mana Cake", 3, 6);
		Item backstageItem = new BackstagePass("Backstage passes to a TAFKAL80ETC concert", 5, 49);
		assertEquals(true, genericItem.Salable() && conjuredItem.Salable() && backstageItem.Salable());
	}
		
	
	/**
	 * "Backstage passes", like aged brie, increases in Quality as its SellIn value approaches;
	 */
	public void testBackstagePassIncreaseInQualityBeforeSellin() {
		Item [] items = new Item [] { new BackstagePass("Backstage passes to a TAFKAL80ETC concert", 12, 40)};
		GildedRose app = new GildedRose(items);
		app.updateQuality();
		assertEquals(41, items[0].GetQuality());
	}

	/**
	 *  "Backstage passes", Quality increases by 2 when there are 10 days or less 
	 */ 
	public void testBackstagePassQualityTimeOne() 
	{
		Item [] items = new Item [] { new BackstagePass("Backstage passes to a TAFKAL80ETC concert", 12, 32)};
		GildedRose app = new GildedRose(items);
		int days = 4;
		for(int i = 0; i <  days  ; i++ ) //add more days more to increase quality between 10 and 5 days before sellin  (9 days before)
		{ 
			app.updateQuality();
		}
		assertEquals(38, items[0].GetQuality());
	}

	/**
	 *  "Backstage passes", Quality increases by 3 when there are 5 days or less
	 */ 
	public void testBackstagePassQualityTimeTwo() 
	{
		Item [] items = new Item [] { new BackstagePass("Backstage passes to a TAFKAL80ETC concert", 12, 31)};
		GildedRose app = new GildedRose(items);
		int days = 9;
		for(int i = 0; i <  days  ; i++ ) //add more days more to increase quality between 10 and 5 days before sellin  (4 days before)
		{ 
			app.updateQuality();
		}
		assertEquals(49, items[0].GetQuality());
	}

	/**
	 * "Backstage passes", Quality drops to 0 after the concert 
	 */
	public void testBackstagePassQualityDayZero() 
	{
		Item [] items = new Item [] { new BackstagePass("Backstage passes to a TAFKAL80ETC concert", 12, 49)};
		GildedRose app = new GildedRose(items);
		int days = 12;
		for(int i = 0; i <=  days  ; i++ ) //add more days more to 0 (concert day)
		{ 
			app.updateQuality();
		}
		assertEquals(0, items[0].GetQuality());
	}

	/**
	 * "Conjured" items degrade in Quality twice as fast as normal items
	 */
	public void testConjuredQualityDecrease()
	{
		Item [] items = new Item[] { new ConjuredItem("Conjured Mana Cake", 3, 6) };
		GildedRose app = new GildedRose(items);
		app.updateQuality();
		assertEquals(3, items[0].GetQuality());
	}
}
