package com.ba.gildedrose;

import java.util.Arrays;

import com.ba.gildedrose.Dao.ItemDAO;
import com.ba.gildedrose.Item.*;


/**
 * Main class
 */
public class App 
{
    /**
     * Access to itemDAO from everywhere in the app
     */
    public static ItemDAO itemDAO;

    public static void main( String[] args )
    {
        // instanciate dependencies
        itemDAO = new ItemDAO(); 

        // call web api routes
        Routes.ProvideRoutes(); 

        // GildedRose app = new GildedRose(itemDAO.getAllItems());

        //#region evaluate values in console
        // int days = 20;

        // for (int i = 0; i < days; i++) {
        //     System.out.println("-------- day " + i + " --------");
        //     System.out.println("name, sellIn, quality");
        //     for (Item item : itemDAO.getAllItems()) {
        //         System.out.println(item);
        //     }
        //     System.out.println();
        //     app.updateQuality();
        // }
        //#endregion
    }
}
