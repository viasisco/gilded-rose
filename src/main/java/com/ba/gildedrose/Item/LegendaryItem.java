package com.ba.gildedrose.Item;

/**
 * Defines the specificity of kind of item, the legendary item.
 * @see Item
 */
public class LegendaryItem extends Item  
{
    //#region ################## CONSTRUCTOR ################# 
    /**
     * Constructor
     * @param name item name
     * @param sellIn item duration salable
     * @param quality item quality
     */
    public LegendaryItem(String name, int sellIn, int quality) 
    {
        super(name, sellIn, quality);
        this._shapeType = "legendary";
    }
    //#endregion

    //#region ######### ACCESSORS ################# 
	/**
	 * The quality is always about 80 for this item and never decreases
     * @param quality legendary item quality
	 */
    @Override
    public void SetQuality(int quality) 
    {
    	// FIXME don't care quality variable or generate exception?
    	this._quality = 80;
    }
    //#endregion

    //#region ################ METHODS ###################### 

    /**
     * update the item quality. A legendary item never decreases. 
     */
    @Override
    public void UpdateItemQuality() 
    {
        //do nothing to stay to 80
    }

    /**
     * Methods which defines if this item is salable
     * @return false
     */
    @Override
    public boolean Salable() 
    {
        return false;
    }
    //#endregion
}