package com.ba.gildedrose.Item;

/**
 * Class which represents a generic item. It's not abstract to allow generic item instanciation
 *
 */
public class Item implements IItem 
{
    
    //#region ############# VARIABLES #######################
	/**
	 * Name item
	 */
    protected String _name;

    /**
     * the number of days we have to sell the item
     */
    protected int _sellIn;

    /**
     * item quality
     */
    protected int _quality; 

    /**
     * item type
     */
    protected String _shapeType;

    //#endregion
    
    //#region ################ ACCESSORS ######################

    // GETTERS

    /**
     * Get the item name
     * @return item name
     */
    public String GetName() 
    { 
    	return this._name;
    }
    
    /**
     * Get the number of days we have to sell the item
     * @return number of days
     */
    public int GetSellin() 
    {
    	return this._sellIn;
    }
    
    /**
     * Get item quality
     * @return item quality
     */
    public int GetQuality() 
    {
    	return this._quality;
    }
    
    // SETTERS
    
    /**
     * Set the item name
     * @param name item name
     */
    public void SetName(String name ) 
    {
    	this._name = name;
    }
    
    /**
     * Defines item's quality. 
     * Quality can't be over 50 and under 0
     * @param quality item quality
     */
    public void SetQuality(int quality) 
    {
        if (quality < 0 ) 
        { 
    		this._quality = 0;	
    	}
        else if (quality > 50) 
        { 
    		this._quality = 50;
    	}
        else this._quality = quality;
    }
    
	/**
	 * Defines the days before expiration
	 * @param sellIn days before expiration
	 */
    public void SetSellin(int sellIn) 
    { 
        this._sellIn = sellIn; 
    }

    //#endregion

    //#region ########################  CONSTRUCTOR #################
    /**
     * Constructor
     * @param name item name
     * @param sellIn days before expiration
     * @param quality item quality
     */
    public Item(String name, int sellIn, int quality) 
    {
        this.SetName(name);
        this.SetSellin(sellIn); // do it before to setQuality because this last depends of sellin
        this.SetQuality(quality);
        this._shapeType = "item";
    }

    //#endregion

    //#region ######################### METHODS ######################### 
    /**
     * Generate item description
     */
    @Override
    public String toString() 
    {
        return this._name + ", " + this._sellIn + ", " + this._quality;
    }

    /**
     * Defines if item is salable. A generic item can be saled.
     */
    public boolean Salable() 
    {
		return true;
	}

    /**
     * update the item quality
     * 
     */
    public void UpdateItemQuality() 
    {
        if (!this._name.equals("Aged Brie")) //TODO create specific item for cheese to refine
        {
            if (this._quality > 0) // quality can't be negative
            {
                if (this._sellIn < 0) 
                {
                    this._quality /= 2;
                }
                else 
                {
                    --this._quality;
                }
            }
        } 
        else 
        {
            if (this._quality < 50) 
            {
                ++this._quality;   
            }
        }
    }
    //#endregion

}


