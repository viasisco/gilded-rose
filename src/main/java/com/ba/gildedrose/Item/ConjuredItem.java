package com.ba.gildedrose.Item;

/**
 * Defines the specificity of kind of item, the conjured item.
 * @see Item
 */
public class ConjuredItem extends Item   
{
	//#region ################## CONSTRUCTOR ################# */
	/**
	 * Constructor (inherit)
	 * @param name item name
	 * @param sellIn day before expiration
	 * @param quality item quality
	 */
	public ConjuredItem(String name, int sellIn, int quality) 
	{
		super(name, sellIn, quality);
		this._shapeType = "conjured";
	}
	//#endregion

	//#region ################ METHODS ###################### 
	/**
     * update the conjured item quality which degrades twice as fast
     */
	@Override
    public void UpdateItemQuality() 
    {
		if (this._quality > 0) 
		{
			this._quality /= 2;
		}
	}
	//#endregion
}