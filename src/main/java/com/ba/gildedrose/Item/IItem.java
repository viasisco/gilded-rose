package com.ba.gildedrose.Item;

/**
 * Item Interface which defines the kind of contrat that each classes will have to implement
 */
public interface IItem 
{
    /**
     * Defines if the item can be saled or not
     * @return boolean item salable
     */
    public boolean Salable();

    /**
     * Methods which defines quality
     */
    public void UpdateItemQuality(); 
}