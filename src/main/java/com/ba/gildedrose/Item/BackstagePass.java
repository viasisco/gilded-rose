package com.ba.gildedrose.Item;

/**
 * Defines the specificity of kind of item, the backstage pass.
 * @see Item
 */
public class BackstagePass extends Item 
{
    //#region ############### CONSTRUCTOR ################ */
    /**
     * Constructor
     * @param name item name
     * @param sellIn item duration salable
     * @param quality item quality
     */
    public BackstagePass(String name, int sellIn, int quality) 
    {
        super(name, sellIn, quality);
        this._shapeType = "backstagepass";
    }
    //#endregion

    //#region ################ METHODS ###################### 
    /**
     * Define the process to specific update for this kind of item
     * Quality increases by 2 when there are 10 days or less 
     * and by 3 when there are 5 days or less 
     * but Quality drops to 0 after the concert
     */
    @Override
    public void UpdateItemQuality() 
    {
        if (this._sellIn > 10 ) 
        {
            this.SetQuality(++this._quality);
            
    	}
        else if (this._sellIn <= 10 && this._sellIn > 5) 
        {
            this.SetQuality(this._quality += 2);
        }
        else if (this._sellIn <= 5 && this._sellIn > 0) 
        {
            this.SetQuality(this._quality += 3);
    	}
        else 
        {
            this.SetQuality(0); 
        }
    }
    //#endregion
}