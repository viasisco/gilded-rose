package com.ba.gildedrose;

import static spark.Spark.*;

import com.ba.gildedrose.Controllers.DefaultController;
import com.ba.gildedrose.Controllers.ItemController;
import com.ba.gildedrose.Item.Item;
import com.google.gson.Gson;

/**
 * Class to manage all web api routes
 * //TODO modify spark uri to give access to the api by this kind of uri : https://myapi/api/v1/stuff
 */
public final class Routes 
{
    /**
     * Private constructor to prevent instantiation
     */
    private Routes() { }

    /**
     * Provide all routes needed for the Web REST API
     */
    public static void ProvideRoutes() 
    {
        // Allow CORS in spark
        options("/*", (req, res) -> {
            String accessControlRequestHeaders = req.headers("Access-Control-Request-Headers");
            if (accessControlRequestHeaders != null) {
                res.header("Access-Control-Allow-Headers", accessControlRequestHeaders);
            }
    
            String accessControlRequestMethod = req.headers("Access-Control-Request-Method");
            if (accessControlRequestMethod != null) {
                res.header("Access-Control-Allow-Methods", accessControlRequestMethod);
            }
    
            return "OK";
        });
    
        before((req, res) -> {
            res.header("Access-Control-Allow-Origin", "*");
            res.header("Access-Control-Allow-Headers", "*");
            res.type("application/json");
        });

        // ROUTES
        get("/", DefaultController.GetByDefault);
        get("/item", ItemController.GetAllItems);   
        //TODO add other routes and request
    }
}