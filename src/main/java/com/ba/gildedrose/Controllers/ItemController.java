package com.ba.gildedrose.Controllers;


import static com.ba.gildedrose.App.itemDAO;

import com.ba.gildedrose.Item.Item;
import com.google.gson.Gson;

import spark.Request;
import spark.Response;
import spark.Route;

/**
 * Web api Item controller to redirect web item requests
 */
public class ItemController 
{

    /**
     * Constructor (avoid instanciation)
     */
    private ItemController() {}

    /**
     * Get all items or filtered items
     * 
     */
    public static Route GetAllItems = (Request req, Response res) -> 
    {
        res.type("application/json");
        Item [] items;
        if (!req.queryParams().isEmpty() && !req.queryParams("filter").isEmpty()) 
        {
            switch (req.queryParams("filter")) 
            {
                case "legendary":
                    return "legendary"; //TODO filter only legendary items
                case "conjured" : //TODO filter only conjured items

                    break;
                case "backstagepass": //TODO filter only backstage items

                    break;
                case "day" : 
                    items = itemDAO.UpdateQuality();
                    break;
                default: 
                    return "not recognized";
            }
        } 
            items = itemDAO.getAllItems();
            return new Gson().toJson(items); 
    };
}