package com.ba.gildedrose.Controllers;

import spark.Request;
import spark.Response;
import spark.Route;

/**
 * Default web request route
 */
public class DefaultController 
{
    /**
     * Constructor (avoid instantiation)
     */
    private DefaultController() {}

    public static Route GetByDefault = (Request req, Response res) -> 
    {
        return "Gilded Rose Inn";
    };


}