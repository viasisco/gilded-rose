package com.ba.gildedrose;

import com.ba.gildedrose.Item.Item;

/**
 * Defines inn workflow
 */
public class GildedRose 
{
    Item[] items; //item array which stores any king of items (polymorphism)

    public GildedRose(Item[] items) 
    {
        this.items = items;
    }

    /**
     * Update quality (typically at the end of the day)
     */
    public void updateQuality() 
    { 
        for (int i = 0; i < items.length; i++) 
        {    
            items[i].UpdateItemQuality();

            items[i].SetSellin(items[i].GetSellin() - 1);
            
            if (items[i].GetSellin() < 0) 
            {
                items[i].UpdateItemQuality();
            }
        }
    }
}
