package com.ba.gildedrose.Dao;

import com.ba.gildedrose.GildedRose;
import com.ba.gildedrose.Item.BackstagePass;
import com.ba.gildedrose.Item.ConjuredItem;
import com.ba.gildedrose.Item.Item;
import com.ba.gildedrose.Item.LegendaryItem;

/**
 * Get Data from sources (files, database, services)
 */
public class ItemDAO 
{
    /**
     * Items data
     */
    private final  Item[] items = new Item[] {
        new Item("+5 Dexterity Vest", 10, 20), //
        new Item("Aged Brie", 2, 0), //
        new Item("Elixir of the Mongoose", 5, 7), //
        // the following items do not work properly yet
        new LegendaryItem("Sulfuras, Hand of Ragnaros", 0, 80), //
        new LegendaryItem("Sulfuras, Hand of Ragnaros", -1, 80),
        new BackstagePass("Backstage passes to a TAFKAL80ETC concert", 15, 20),
        new BackstagePass("Backstage passes to a TAFKAL80ETC concert", 10, 49),
        new BackstagePass("Backstage passes to a TAFKAL80ETC concert", 5, 49),
        new ConjuredItem("Conjured Mana Cake", 3, 6) };

        /**
         * Get all items of each kind
         * @return items collection of items
         */
        public Item[] getAllItems() 
        {
            return items;
        }

        /**
         * Call gilded rose to update quality by adding a day
         * @return
         */
        public Item[] UpdateQuality() 
        {
            GildedRose app = new GildedRose(items);
            app.updateQuality();
            return items;
        }
}