import { Component, OnInit } from '@angular/core';
import { ItemService } from 'src/app/Services/item.service';

@Component({
  selector: 'app-item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.css']
})

/**
 * Component which displays items
 */
export class ItemComponent implements OnInit 
{

  /**
   * items arrays (of any kind)
   */
  public items : any;

  /**
   * number of days
   */
  public days : number = 0;
  
  /**
   * Constructor which instanciates services et get data for values
   * @param _itemService instanciate item service
   */
  constructor( 	private _itemService: ItemService) 
  {
     this.GetData();
  }

  /** Angular life cycle  */
  ngOnInit() { }

  /**
   * Get All items 
   */
  GetData() 
  {
    this._itemService
    .GetArrayItemList()
    .subscribe(result => 
      {
        this.items = result;
        this.EvaluateBestQuality();
      });
  }

  /**
   * Call services to update the items array 
   * (the logic is on the back)
   */
  public passADay() 
  {
    this._itemService
    .UpdateQuality()
    .subscribe(result => 
      {
        this.items = result;
        this.EvaluateBestQuality();
      });
    this.days++;
  }

  /**
   * Evaluate the best quality for each item
   */
  private EvaluateBestQuality() 
  {
    var lastItem = 0;
    var lastLegendary = 0;
    var lastBackstagepass = 0;
    var lastConjured = 0;
    //stores index of each kind of item best quality. Index 0 for item, 1, for legendary item, 2 for backstagepass item and 3 for conjured item
    var itemQuality :number[] = new Array(4); //TODO improve by a dictionnary by key/item.
    for (let i = 0; i < this.items.length; i++) 
    {
      if (this.items[i]._shapeType === "item" && this.items[i]._quality >= lastItem ) 
      {
        lastItem = this.items[i]._quality;
        itemQuality[0] = i;
      }
      else if (this.items[i]._shapeType === "legendary" && this.items[i]._quality >= lastLegendary) 
      {
        lastLegendary = this.items[i]._quality;
        itemQuality[1] = i;;
      }
      else if (this.items[i]._shapeType === "backstagepass" && this.items[i]._quality >= lastBackstagepass) 
      {
        lastBackstagepass = this.items[i]._quality;
        itemQuality[2] = i;
      }
      else if (this.items[i]._shapeType === "conjured" && this.items[i]._quality >= lastConjured) 
      {
        lastConjured = this.items[i]._quality;
        itemQuality[3] = i;
      }
      this.items[i].bestQuality = false;
    }

    itemQuality.forEach(element => 
    {
      this.items[element].bestQuality = true;
    });
  }
}
