import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './Components/app.component';
import { ItemComponent } from './Components/item/item.component';

@NgModule({
  declarations: [
    AppComponent,
    ItemComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule, 
    HttpClientModule
  ],
  providers: [         
    {
      provide: 'BASE_URL', useFactory: getBaseUrl
    }
  ],
  bootstrap: [AppComponent],
  exports: [ItemComponent]
})


export class AppModule { }

/**
 * One place to store the base URL API
 */
export function getBaseUrl() {
  return "http://localhost:4567/";
}