import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})

/**
 * Service to manage web requests for items
 */
export class ItemService {

  /**
   * URL to call for web request API
   */
  private _baseUrl: string;

  /**
   * Constructor which instanciate baseurl and the httpclient lib
   * @param baseUrl API url for web requests
   * @param _httpClient http client lib
   */
  // constructor(private _httpClient: HttpClient) 
  constructor(@Inject('BASE_URL') baseUrl: string, private _httpClient: HttpClient) 
  {
    this._baseUrl = baseUrl;
  }

  /**
   * Get items array
   * @returns items items Array
   */
  public GetArrayItemList()
  {
    return this._httpClient.get<any>(this._baseUrl + 'item'); //TODO create a type result (interface) with status, message et data attributes
  }

  /**
   * Get items array with a quality update
   * The logic stay on the back to not be duplicated
   * @return items items array 
   */
  public UpdateQuality() 
  {
    return this._httpClient.get<any>(this._baseUrl + 'item?filter=day');
  }
}
