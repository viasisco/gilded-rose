## Description of the process
- [X] Setup the project with build and version control, preferably with Maven and git.
- [X] Publish it on a version control platform for the reviewers to access (github, gitlab, etc).
- [X] On top of the existing implementation, add the 3 following independent items:
 - Legendary items, Backstage passes, Conjured items (cf requirements),
- [X] Add test file to get TDD design
- [X] The Golden Master is there for reference of an acceptable item creation API but can be changed if desired.
- Give to the user the ability to view the content of a virtual inn's inventory by:
 - [X] Exposing a REST-like API to retrieve the information,
 - [X] Providing a minimal Web UI to preview the items and their characteristics,
  - Aesthetics have no importance at all in this task.
- [X] Give to the user the ability to express, via this UI, that a day has passed so that the items degrades as expected.
- [X] Add a functionality that let the user know, in the inventory, which item is of the best quality for every item type
- As a simplification, we consider that two items with the same name has the same type.


## Tools used 
- maven version 3.5.4
- JDK version 1.8.0_191
- Spark version 2.7.2
- Angular version 6.1.0
- visual studio code 
- vs extensions : 
    - maven for java
    - language support for java by redhat
    - java test runner
    - java extension pack
    - debugger for java
    

## Useful commands
### Backend REST API (tags : java, maven, spark)
Generate the project 

`mvn archetype:generate -DgroupId=com.ba.gildedrose -DartifactId=gilded-rose -DarchetypeArtifactId=maven-archetype-quickstart -DinteractiveMode=false`


Test, verify, compile, package

`mvn test`

`mvn verify`

`mvn compile`

`mvn package`

Launch

`java -cp .\target\gilded-rose-1.0-SNAPSHOT.jar com.ba.gildedrose.App`

Generate documentation (stored in target/site/apidocs)

`mvn javadoc:javadoc`


### Frontend (tags: angular, nodejs, npm)

Require : Nodejs/npm

Install angular-cli : 

`npm install -g @angular/cli`


Generate the project in production mode
change directory to client/

`npm install`

`ng build --prod` 


## How to launch the project
This project includes two separates projects : a backend REST API and a frontend. The frontend, is stored in the client folder. To launch the project, it's necessary to launch the two projects, on differents ports. 

**In first**, launch the backend REST API: 

`java -cp .\target\gilded-rose-1.0-SNAPSHOT.jar com.ba.gildedrose.App`

Now, take access by the url (but it's just the API access) : 
` http://localhost:4567/ `

**In a second time**, launch the frontend (in debug mode, to not have to deploy it)  : 

` cd client/ `

`npm install` //download each librairies (nodes_modules) needed by the project

`ng serve` 

Now, take access by the url : 
`http://localhost:4200` 




